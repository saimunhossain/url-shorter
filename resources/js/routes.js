import UrlShorter from './components/UrlShorter.vue';
  
export const routes = [
    {
        name: 'home',
        path: '/',
        component: UrlShorter
    }
];