<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UrlShorterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::get('/get-url-list', [UrlShorterController::class,'index']);
    Route::post('generate-shorten-link',  [UrlShorterController::class, 'store'])->name('generate.shorten.link.post');
    Route::get('/single-code/{code?}',  [UrlShorterController::class,'shortenLink'])->name('shorten.link');
});
