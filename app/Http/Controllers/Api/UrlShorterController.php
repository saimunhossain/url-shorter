<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UrlShorterStoreRequest;
use App\Service\UrlShorter\UrlShorterService;

class UrlShorterController extends Controller
{
    protected $urlShorterService;

    public function __construct(UrlShorterService $urlShorterService)
    {
        $this->urlShorterService = $urlShorterService;
    }
    public function index()
    {
        return $this->urlShorterService->getAllShortUrl();
    }

    public function store(UrlShorterStoreRequest $request)
    {
        return $this->urlShorterService->storeShortUrl($request);
    }

    public function shortenLink($shortUrlCode)
    {
        return $this->urlShorterService->getSingleShortUrl($shortUrlCode);
    }
}
