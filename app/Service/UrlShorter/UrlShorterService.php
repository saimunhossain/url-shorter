<?php

namespace App\Service\UrlShorter;
use App\Models\UrlShorter;
use Illuminate\Support\Str;
use App\Http\Resources\UrlShorterResource;

class UrlShorterService
{
    public function getAllShortUrl()
    {
        $shortLinks = UrlShorter::latest()->get();
        return UrlShorterResource::collection($shortLinks);
    }

    public function storeShortUrl($request)
    {
        $input['link'] = $request->link;
        $this->checkGoogleSafeBrowsingApi($request->link);
        $input['code'] = $this->hashGenerator(6);
        $shortLink = UrlShorter::create($input);
        return response()->json([
            'status' => 'success',
            'shortlink'   => $shortLink
        ]);
    }

    private function checkGoogleSafeBrowsingApi($requeatUrl)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://safebrowsing.googleapis.com/v4/threatMatches:find?key=AIzaSyCIhxjf0twh4oKE__xqoNgLxl8r18YnvGM",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => '{
                    "client": {
                        "clientId":      "UrlShorter",
                        "clientVersion": "1.5.2"
                    },
                    "threatInfo": {
                        "threatTypes":      ["MALWARE", "SOCIAL_ENGINEERING"],
                        "platformTypes":    ["WINDOWS"],
                        "threatEntryTypes": ["URL"],
                        "threatEntries": [
                            "url" => '.$requeatUrl.'
                        ]
                    }
            }',
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        echo "cURL Error #:" . $err;
        } else {
        echo $response;
        }
    }

    private function hashGenerator($stringLength)
    {
        return Str::random($stringLength);
    }

    public function getSingleShortUrl($shortUrlCode)
    {
        $singleShortUrl = UrlShorter::where('code', $shortUrlCode)->first();
        return new UrlShorterResource($singleShortUrl);
    }
}