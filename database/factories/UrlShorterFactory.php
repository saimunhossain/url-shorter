<?php
use Faker\Generator as Faker;

$factory->define(App\Models\ShortLink::class, function (Faker $faker) {
    return [
        'code' => $faker->regexify('[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}'),
        'link' => $faker->url()
    ];
});