<?php

namespace Tests\Unit;
use Faker\Generator as Faker;
use App\Models\ShortLink;
use PHPUnit\Framework\TestCase;

class UrlShorterTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->assertTrue(true);
    }

    public function test_get_all_short_url()
    {
        $factory = new Faker;
        $urlShorters = $factory->create(ShortLink::class, 2)->create()->map(function ($urlShorter) {
            return $urlShorter->only(['id', 'code', 'link']);
        });

        $this->get(route('/api/v1/get-url-list'))
            ->assertStatus(200)
            ->assertJson($urlShorters->toArray())
            ->assertJsonStructure([
                '*' => [ 'id', 'code', 'link' ],
            ]);
    }
}
